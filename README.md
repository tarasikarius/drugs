Installation:
    
   - docker-compose up -d
   - docker exec drugs_php-fpm composer install
   - docker exec drugs_php-fpm bin/console doctrine:migrations:migrate -n
   - docker exec drugs_php-fpm bin/console doctrine:fixtures:load -n

Open http://localhost:8080 in browser
