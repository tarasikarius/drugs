<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class PriceExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('price', [$this, 'convertPrice']),
        ];
    }

    public function convertPrice($coins): float
    {
        return $coins / 100;
    }
}
