<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Drug
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $name;

    /**
     * Цена (в копейках)
     *
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=ActiveSubstance::class)
     * @ORM\JoinColumn(nullable=false)
     *
     * @var ActiveSubstance
     */
    private $activeSubstance;

    /**
     * @ORM\ManyToOne(targetEntity=Manufacturer::class)
     * @ORM\JoinColumn(nullable=false)
     *
     * @var Manufacturer
     */
    private $manufacturer;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice():?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getActiveSubstance(): ?ActiveSubstance
    {
        return $this->activeSubstance;
    }

    public function setActiveSubstance(ActiveSubstance $activeSubstance): self
    {
        $this->activeSubstance = $activeSubstance;

        return $this;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(Manufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }
}
