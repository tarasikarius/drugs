<?php

namespace App\DataFixtures;

use App\Factory\DrugFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        DrugFactory::createMany(30);

        $manager->flush();
    }
}
