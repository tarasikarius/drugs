<?php

namespace App\Factory;

use App\Entity\Drug;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Drug|Proxy createOne(array $attributes = [])
 * @method static Drug[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static Drug|Proxy find($criteria)
 * @method static Drug|Proxy findOrCreate(array $attributes)
 * @method static Drug|Proxy first(string $sortedField = 'id')
 * @method static Drug|Proxy last(string $sortedField = 'id')
 * @method static Drug|Proxy random(array $attributes = [])
 * @method static Drug|Proxy randomOrCreate(array $attributes = [])
 * @method static Drug[]|Proxy[] all()
 * @method static Drug[]|Proxy[] findBy(array $attributes)
 * @method static Drug[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Drug[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method Drug|Proxy create($attributes = [])
 */
final class DrugFactory extends ModelFactory
{
    private const DRUG_NAMES = ['Pegadol', 'Tropsalstatin', 'Rifaaparinux', 'Pegicaserin', 'Gliavintraposin', 'Tropagrelapril', 'Somapristin', 'Pegasermin', 'Ioracetam', 'Virsalbersat', 'Vinasaltocin'];

    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->randomElement(self::DRUG_NAMES),
            'price' => self::faker()->numberBetween(30000, 900000),
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this
            ->afterInstantiate(function(Drug $drug) {
                $drug->setActiveSubstance(ActiveSubstanceFactory::new()->create()->object());
                $drug->setManufacturer(ManufacturerFactory::new()->create()->object());
            })
        ;
    }

    protected static function getClass(): string
    {
        return Drug::class;
    }
}
