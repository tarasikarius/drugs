<?php

namespace App\Factory;

use App\Entity\Manufacturer;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Manufacturer|Proxy createOne(array $attributes = [])
 * @method static Manufacturer[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static Manufacturer|Proxy find($criteria)
 * @method static Manufacturer|Proxy findOrCreate(array $attributes)
 * @method static Manufacturer|Proxy first(string $sortedField = 'id')
 * @method static Manufacturer|Proxy last(string $sortedField = 'id')
 * @method static Manufacturer|Proxy random(array $attributes = [])
 * @method static Manufacturer|Proxy randomOrCreate(array $attributes = [])
 * @method static Manufacturer[]|Proxy[] all()
 * @method static Manufacturer[]|Proxy[] findBy(array $attributes)
 * @method static Manufacturer[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Manufacturer[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method Manufacturer|Proxy create($attributes = [])
 */
final class ManufacturerFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->unique()->company(),
            'website' => self::faker()->unique()->url(),
        ];
    }

    protected static function getClass(): string
    {
        return Manufacturer::class;
    }
}
