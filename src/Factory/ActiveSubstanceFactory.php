<?php

namespace App\Factory;

use App\Entity\ActiveSubstance;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static ActiveSubstance|Proxy createOne(array $attributes = [])
 * @method static ActiveSubstance[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static ActiveSubstance|Proxy find($criteria)
 * @method static ActiveSubstance|Proxy findOrCreate(array $attributes)
 * @method static ActiveSubstance|Proxy first(string $sortedField = 'id')
 * @method static ActiveSubstance|Proxy last(string $sortedField = 'id')
 * @method static ActiveSubstance|Proxy random(array $attributes = [])
 * @method static ActiveSubstance|Proxy randomOrCreate(array $attributes = [])
 * @method static ActiveSubstance[]|Proxy[] all()
 * @method static ActiveSubstance[]|Proxy[] findBy(array $attributes)
 * @method static ActiveSubstance[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ActiveSubstance[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method ActiveSubstance|Proxy create($attributes = [])
 */
final class ActiveSubstanceFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->unique()->word(),
        ];
    }

    protected static function getClass(): string
    {
        return ActiveSubstance::class;
    }
}
