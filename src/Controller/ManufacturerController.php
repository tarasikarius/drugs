<?php

namespace App\Controller;

use App\Entity\Manufacturer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/manufacturer")
 */
class ManufacturerController extends AbstractController
{
    /**
     * @Route("/", name="manufacturer_index", methods={"GET"})
     */
    public function index(): Response
    {
        $manufacturers = $this->getDoctrine()
            ->getRepository(Manufacturer::class)
            ->findAll();

        return $this->render('manufacturer/index.html.twig', [
            'manufacturers' => $manufacturers,
        ]);
    }
}
