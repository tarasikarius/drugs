<?php

namespace App\Controller;

use App\Entity\ActiveSubstance;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/active/substance")
 */
class ActiveSubstanceController extends AbstractController
{
    /**
     * @Route("/", name="active_substance_index", methods={"GET"})
     */
    public function index(): Response
    {
        $activeSubstances = $this->getDoctrine()
            ->getRepository(ActiveSubstance::class)
            ->findAll();

        return $this->render('active_substance/index.html.twig', [
            'active_substances' => $activeSubstances,
        ]);
    }
}
