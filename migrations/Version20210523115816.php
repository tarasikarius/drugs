<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210523115816 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE active_substance_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE drug_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE manufacturer_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE active_substance (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F217882D5E237E06 ON active_substance (name)');
        $this->addSql('CREATE TABLE drug (id INT NOT NULL, active_substance_id INT NOT NULL, manufacturer_id INT NOT NULL, name VARCHAR(255) NOT NULL, price INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_43EB7A3E265D87F5 ON drug (active_substance_id)');
        $this->addSql('CREATE INDEX IDX_43EB7A3EA23B42D ON drug (manufacturer_id)');
        $this->addSql('CREATE TABLE manufacturer (id INT NOT NULL, name VARCHAR(255) NOT NULL, website VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3D0AE6DC5E237E06 ON manufacturer (name)');
        $this->addSql('ALTER TABLE drug ADD CONSTRAINT FK_43EB7A3E265D87F5 FOREIGN KEY (active_substance_id) REFERENCES active_substance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE drug ADD CONSTRAINT FK_43EB7A3EA23B42D FOREIGN KEY (manufacturer_id) REFERENCES manufacturer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE drug DROP CONSTRAINT FK_43EB7A3E265D87F5');
        $this->addSql('ALTER TABLE drug DROP CONSTRAINT FK_43EB7A3EA23B42D');
        $this->addSql('DROP SEQUENCE active_substance_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE drug_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE manufacturer_id_seq CASCADE');
        $this->addSql('DROP TABLE active_substance');
        $this->addSql('DROP TABLE drug');
        $this->addSql('DROP TABLE manufacturer');
    }
}
